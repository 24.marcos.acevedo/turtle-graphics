import turtle
turtle.setup(800,600)
t=turtle.Turtle()
t.speed(10)

circle_positions = [ (-120,0,"blue"), (0,0,"black"), (120,0,"red"),
                   (-60,-60, "yellow"), (60,-60, "green") ]

for pos in circle_positions:
	t.penup()
	t.setpos(pos[0], pos[1])
	t.pencolor(pos[2])
	t.pensize(5)
	t.pendown()
	t.circle(50)

turtle.done()
