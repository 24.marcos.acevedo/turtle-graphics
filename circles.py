
import turtle
import random

turtle.setup (600,400)
t=turtle.Turtle()
t.speed(0)
colours = ["cyan", "purple", "black", "blue"]

#Tangent circles
for i in range(1,20):

 
 t.circle(i*10)
 t.color(random.choice(colours))
 
turtle.done()

